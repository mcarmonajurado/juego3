#include "CreditsState.h"

#include "IntroState.h"

template<> CreditsState* Ogre::Singleton<CreditsState>::msSingleton = 0;
const Ogre::String SONG = "windloop.ogg";

void
CreditsState::enter ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");
    Ogre::LogManager::getSingletonPtr()->logMessage("CreditsState::enter");
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");

    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    // Nuevo background colour.
    _viewport->setBackgroundColour(Ogre::ColourValue(1.0, 0.0, 0.0));

    _exitGame = false;

    buildGUI();

    // Carga del sonido.
    TrackPtr mainTrack = GameManager::getSingletonPtr()->getTrackManager()->load(SONG);
    GameManager::getSingletonPtr()->setTrackPtr(mainTrack);

    // Reproducción del track principal...
    mainTrack->play();
}

void
CreditsState::exit ()
{
    if(!GameManager::getSingletonPtr()->getTrackPtr().isNull())
        GameManager::getSingletonPtr()->getTrackPtr()->stop();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();

    if(GameManager::getSingletonPtr()->getTrayManager()){
        GameManager::getSingletonPtr()->getTrayManager()->clearAllTrays();
        GameManager::getSingletonPtr()->getTrayManager()->destroyAllWidgets();
        GameManager::getSingletonPtr()->getTrayManager()->setListener(0);
    }
}

void
CreditsState::pause ()
{
}

void
CreditsState::resume ()
{
    buildGUI();
}

bool
CreditsState::frameStarted
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);
    return true;
}

bool
CreditsState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
CreditsState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);

    if (_exitGame)
        return false;

    return true;
}

void
CreditsState::keyPressed
(const OIS::KeyEvent &e) {
    // Tecla p --> Estado anterior.
    if (e.key == OIS::KC_A) {
        Ogre::LogManager::getSingletonPtr()->logMessage("Estado anterior");
        popState();
    }
}

void
CreditsState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
CreditsState::mouseMoved
(const OIS::MouseEvent &e)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseMove(e);
}

void
CreditsState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseDown(e, id);
}

void
CreditsState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseUp(e, id);
}

CreditsState*
CreditsState::getSingletonPtr ()
{
    return msSingleton;
}

CreditsState&
CreditsState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void CreditsState::buildGUI()
{
    OgreBites::SdkTrayManager* trayMgr = GameManager::getSingletonPtr()->getTrayManager();
    trayMgr->setListener(this);
    trayMgr->destroyAllWidgets();
    trayMgr->showCursor();
    trayMgr->createLabel(OgreBites::TL_TOP, "CreditsLbl", "CreditsState", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "BackToMenuBtn", "Volver al Menu", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "ExitBtn", "Salir", 250);

    Ogre::FontManager::getSingleton().getByName("SdkTrays/Caption")->load();
    Ogre::FontManager::getSingleton().getByName("SdkTrays/Value")->load();
}

void CreditsState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "ExitBtn")
        GameManager::getSingletonPtr()->getTrayManager()->showYesNoDialog("¿Estás seguro?", "¿Quieres salir realmente?");
    else if(button->getName() == "BackToMenuBtn")
        popState();
}

void CreditsState::yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit)
{
    if(yesHit == true)
        _exitGame = true;
    else
        GameManager::getSingletonPtr()->getTrayManager()->closeDialog();
}
