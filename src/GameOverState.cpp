#include "GameOverState.h"

#include "IntroState.h"

template<> GameOverState* Ogre::Singleton<GameOverState>::msSingleton = 0;

void
GameOverState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    // Nuevo background colour.
    _viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 0.0));

    _exitGame = false;

    buildGUI();
}

void
GameOverState::exit ()
{
    if(!GameManager::getSingletonPtr()->getTrackPtr().isNull())
        GameManager::getSingletonPtr()->getTrackPtr()->stop();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();

    if(GameManager::getSingletonPtr()->getTrayManager()){
        GameManager::getSingletonPtr()->getTrayManager()->clearAllTrays();
        GameManager::getSingletonPtr()->getTrayManager()->destroyAllWidgets();
        GameManager::getSingletonPtr()->getTrayManager()->setListener(0);
    }
}

void
GameOverState::pause ()
{
}

void
GameOverState::resume ()
{
}

bool
GameOverState::frameStarted
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);
    return true;
}

bool
GameOverState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
GameOverState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);

    if (_exitGame)
        return false;

    return true;
}

void
GameOverState::keyPressed
(const OIS::KeyEvent &e) {
    // Tecla p --> Estado anterior.
    if (e.key == OIS::KC_A) {
        popState();
    }
}

void
GameOverState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
GameOverState::mouseMoved
(const OIS::MouseEvent &e)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseMove(e);
}

void
GameOverState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseDown(e, id);
}

void
GameOverState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseUp(e, id);
}

GameOverState*
GameOverState::getSingletonPtr ()
{
    return msSingleton;
}

GameOverState&
GameOverState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void GameOverState::buildGUI()
{
    OgreBites::SdkTrayManager* trayMgr = GameManager::getSingletonPtr()->getTrayManager();
    trayMgr->setListener(this);
    trayMgr->destroyAllWidgets();
    trayMgr->showCursor();
    trayMgr->createLabel(OgreBites::TL_TOP, "GameOverLbl", "GameOverState", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "BackToMenuBtn", "Volver al Menu", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "ExitBtn", "Salir", 250);

    Ogre::FontManager::getSingleton().getByName("SdkTrays/Caption")->load();
    Ogre::FontManager::getSingleton().getByName("SdkTrays/Value")->load();
}

void GameOverState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "ExitBtn")
        GameManager::getSingletonPtr()->getTrayManager()->showYesNoDialog("¿Estás seguro?", "¿Quieres salir realmente?");
    else if(button->getName() == "BackToMenuBtn")
        changeState(IntroState::getSingletonPtr());
}

void GameOverState::yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit)
{
    if(yesHit == true)
        _exitGame = true;
    else
        GameManager::getSingletonPtr()->getTrayManager()->closeDialog();
}
