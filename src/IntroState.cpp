#include "IntroState.h"

#include "PlayState.h"
#include "CreditsState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void
IntroState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();

    // If we returns from another state.
    if(_sceneMgr){
        Ogre::LogManager::getSingletonPtr()->logMessage("Reset SceneManager");
        _root->destroySceneManager(_sceneMgr);
        //_sceneMgr->destroyAllCameras();
        _root->getAutoCreatedWindow()->removeAllViewports();
    }

    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
    _camera = _sceneMgr->createCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));

    _exitGame = false;

    buildGUI();
}

void
IntroState::exit()
{
    if(!GameManager::getSingletonPtr()->getTrackPtr().isNull())
        GameManager::getSingletonPtr()->getTrackPtr()->stop();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();

    if(GameManager::getSingletonPtr()->getTrayManager()){
        GameManager::getSingletonPtr()->getTrayManager()->clearAllTrays();
        GameManager::getSingletonPtr()->getTrayManager()->destroyAllWidgets();
        GameManager::getSingletonPtr()->getTrayManager()->setListener(0);
    }
}

void
IntroState::pause ()
{
}

void
IntroState::resume ()
{
    enter ();
}

bool
IntroState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
IntroState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
IntroState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);
    return true;
}

void
IntroState::keyPressed
(const OIS::KeyEvent &e)
{
    // Transición al siguiente estado.
    // Espacio --> PlayState
    if (e.key == OIS::KC_SPACE) {
        changeState(PlayState::getSingletonPtr());
    }

    if (e.key == OIS::KC_C) {
        pushState(CreditsState::getSingletonPtr());
    }
}

void
IntroState::keyReleased
(const OIS::KeyEvent &e )
{
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void
IntroState::mouseMoved
(const OIS::MouseEvent &e)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseMove(e);
}

void
IntroState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseDown(e, id);
}

void
IntroState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseUp(e, id);
}

IntroState*
IntroState::getSingletonPtr ()
{
    return msSingleton;
}

IntroState&
IntroState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void IntroState::buildGUI()
{
    OgreBites::SdkTrayManager* trayMgr = GameManager::getSingletonPtr()->getTrayManager();
    trayMgr->setListener(this);
    trayMgr->destroyAllWidgets();
    trayMgr->showCursor();
    trayMgr->createLabel(OgreBites::TL_TOP, "MenuLbl", "IntroState", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "EnterBtn", "Iniciar partida", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "CreditBtn", "Créditos", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "ExitBtn", "Salir", 250);

    Ogre::FontManager::getSingleton().getByName("SdkTrays/Caption")->load();
    Ogre::FontManager::getSingleton().getByName("SdkTrays/Value")->load();
}

void IntroState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "ExitBtn")
        _exitGame = true;
    else if(button->getName() == "EnterBtn")
        changeState(PlayState::getSingletonPtr());
    else if(button->getName() == "CreditBtn")
        pushState(CreditsState::getSingletonPtr());
}
