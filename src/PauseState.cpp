#include "PauseState.h"

#include "IntroState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void
PauseState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    // Nuevo background colour.
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 1.0, 0.0));

    _exitGame = false;

    buildGUI();
}

void
PauseState::exit ()
{
    if(!GameManager::getSingletonPtr()->getTrackPtr().isNull())
        GameManager::getSingletonPtr()->getTrackPtr()->stop();

    if(GameManager::getSingletonPtr()->getTrayManager()){
        GameManager::getSingletonPtr()->getTrayManager()->clearAllTrays();
        GameManager::getSingletonPtr()->getTrayManager()->destroyAllWidgets();
        GameManager::getSingletonPtr()->getTrayManager()->setListener(0);
    }
}

void
PauseState::pause ()
{
}

void
PauseState::resume ()
{
}

bool
PauseState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
PauseState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
PauseState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);
    return true;
}

void
PauseState::keyPressed
(const OIS::KeyEvent &e) {
    switch(e.key)
    {
    // Tecla p --> Estado anterior.
    case OIS::KC_P:
        popState();
        break;
    default:
        break;
    }
}

void
PauseState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
PauseState::mouseMoved
(const OIS::MouseEvent &e)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseMove(e);
}

void
PauseState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseDown(e, id);
}

void
PauseState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseUp(e, id);
}

PauseState*
PauseState::getSingletonPtr ()
{
    return msSingleton;
}

PauseState&
PauseState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void PauseState::buildGUI()
{
    OgreBites::SdkTrayManager* trayMgr = GameManager::getSingletonPtr()->getTrayManager();
    trayMgr->setListener(this);
    trayMgr->destroyAllWidgets();
    trayMgr->showCursor();
    trayMgr->createLabel(OgreBites::TL_TOP, "PauseLbl", "Pause", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "BackToGameBtn", "Volver al juego", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "BackToMenuBtn", "Volver al Menu", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "ExitBtn", "Salir del juego", 250);

    Ogre::FontManager::getSingleton().getByName("SdkTrays/Caption")->load();
    Ogre::FontManager::getSingleton().getByName("SdkTrays/Value")->load();
}

void PauseState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "ExitBtn")
        GameManager::getSingletonPtr()->getTrayManager()->showYesNoDialog("¿Estás seguro?", "¿Quieres salir realmente?");
    else if(button->getName() == "BackToGameBtn")
        popState();
    else if(button->getName() == "BackToMenuBtn")
        cleanAndChangeState(IntroState::getSingletonPtr());
}

void PauseState::yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit)
{
    if(yesHit == true)
        _exitGame = true;
    else
        GameManager::getSingletonPtr()->getTrayManager()->closeDialog();
}
