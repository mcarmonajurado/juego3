﻿#include "PlayState.h"
#include "PauseState.h"

#include "WinningState.h"

#include <vector>

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h>

#include <OgreBullet/Dynamics/OgreBulletDynamicsWorld.h>
#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/Debug/OgreBulletCollisionsDebugDrawer.h>

#include <OgreBullet/Dynamics/Constraints/OgreBulletDynamicsRaycastVehicle.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h>
#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>

using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;
const Ogre::String SONG = "windloop.ogg";

void
PlayState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    // Nuevo background colour.
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

    _exitGame = false;
    // ----------------------

    _camera->setPosition(Ogre::Vector3 ( 0.0f, 40.0f, 80.0f ) );
    _camera->lookAt(Ogre::Vector3 ( 0.0f, 13.0f, 0.0f ) );
    _camera->setNearClipDistance(5);
    _camera->setFarClipDistance(10000);
    // ----------------------

    buildGUI();

    // Carga del sonido.
    TrackPtr mainTrack = GameManager::getSingletonPtr()->getTrackManager()->load(SONG);
    GameManager::getSingletonPtr()->setTrackPtr(mainTrack);

    // Reproducción del track principal...
    mainTrack->play();

    //-----------------------------------------------------------------------------

    // Creacion del modulo de debug visual de Bullet
    _debugDrawer = new OgreBulletCollisions::DebugDrawer();
    _debugDrawer->setDrawWireframe(true);
    Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->createChildSceneNode ( "debugNode", Ogre::Vector3::ZERO );
    node->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer));

    // Creacion del mundo (definicion de los limites y la gravedad)
    Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
                Ogre::Vector3 (-100, -100, -100),
                Ogre::Vector3 (100,  100,  100));
    Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

    _world = new OgreBulletDynamics::DynamicsWorld ( _sceneMgr, worldBounds, gravity);
    _world->setDebugDrawer (_debugDrawer);

    _contadorBolas = 0;

    // Creacion de los elementos iniciales del mundo
    createInitialWorld();

    createScene();
}

void
PlayState::exit ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("===============");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::exit");
    Ogre::LogManager::getSingletonPtr()->logMessage("===============");
    if(!GameManager::getSingletonPtr()->getTrackPtr().isNull())
        GameManager::getSingletonPtr()->getTrackPtr()->stop();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _vCoches.clear();

    if(GameManager::getSingletonPtr()->getTrayManager()){
        GameManager::getSingletonPtr()->getTrayManager()->clearAllTrays();
        GameManager::getSingletonPtr()->getTrayManager()->destroyAllWidgets();
        GameManager::getSingletonPtr()->getTrayManager()->setListener(0);
    }
}

void
PlayState::pause()
{
}

void
PlayState::resume()
{
    // Se restaura el background colour.
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

    buildGUI();
}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);
    return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
PlayState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);

    update(evt.timeSinceLastEvent);
    if (_exitGame)
        return false;

    return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
    switch(e.key)
    {
    case OIS::KC_ESCAPE:
        //_exitGame = true;
        pushState(PauseState::getSingletonPtr());
        break;
    case OIS::KC_P:// Tecla p --> PauseState.
        pushState(PauseState::getSingletonPtr());
        break;
    case OIS::KC_R:// resetea el juego
        changeState(PlayState::getSingletonPtr());
        break;
    case OIS::KC_D:
        _world->setShowDebugShapes (true);
        break;
    case OIS::KC_H:
        _world->setShowDebugShapes (false);
        break;
    case OIS::KC_SPACE:
        insertarBolaEscena("Bola");
        break;
    default:
        break;
    }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseMove(e);
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseDown(e, id);
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseUp(e, id);
}

PlayState*
PlayState::getSingletonPtr ()
{
    return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::buildGUI()
{
    OgreBites::SdkTrayManager* trayMgr = GameManager::getSingletonPtr()->getTrayManager();
    trayMgr->setListener(this);
    trayMgr->destroyAllWidgets();
    trayMgr->showCursor();
    trayMgr->createLabel(OgreBites::TL_TOP, "PauseLbl", "PlayState", 250);

    Ogre::FontManager::getSingleton().getByName("SdkTrays/Caption")->load();
    Ogre::FontManager::getSingleton().getByName("SdkTrays/Value")->load();
}

void PlayState::createInitialWorld()
{
    // Activamos las sombras
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    _sceneMgr->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5) );
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.9, 0.9, 0.9));

    _sceneMgr->setShadowTextureCount(2);
    _sceneMgr->setShadowTextureSize(512);

    //Cargamos todos los elementos de la escena

    DotSceneLoader* pDotSceneLoader = new DotSceneLoader();
    pDotSceneLoader->parseDotSceneCustom("posiciones.xml", "General", _sceneMgr, _sceneMgr->getRootSceneNode(),"", _world);
    delete pDotSceneLoader;

//    _sceneMgr->getEntity("athene")->setMaterialName("Examples/Athene/Basic");
//    _sceneMgr->getEntity("athene")->setMaterialName("Examples/Athene/NormalMapped");
//    _sceneMgr->getSceneNode("athene")->setScale(_sceneMgr->getSceneNode("athene")->getScale()/2);

    // Creamos el/los vehiculo/s =============================================

    char name[100];
    eColour_Chassis color = RED;
//    float posX = 13.0f;
//    float posY = 12.0f;
//    float posZ = 0.0f;
//    float base = 7.0f;
    float posX = 0.0f;
    float posY = 0.0f;
    float posZ = -5.0f;
    float base = 7.0f;

    for ( unsigned int i = 0; i < _NUM_COCHES_ ; i++ )
    {
        if ( i % 2 == 0 )
        {
            if ( i > 0 )
                base -= 7.0f;
            posX = 18.0f;
            posZ = base;
        }
        else
        {
            posX = 22.0f;
            posZ = base - 3.0f;
        }

        memset ( name, 0, sizeof(char)*100 );
        sprintf ( name, "Coche%03u", i+1 );
        _vCoches.push_back ( new Coche ( name, 0, 0, 0,  _sceneMgr, _world, color ) );
//        _vCoches.push_back ( new Coche ( name, posX, posY, posZ,  _sceneMgr, _world, color ) );
        _vCoches[i]->print_info();

        if ( i == 0 )
            color = BLUE;
        else if ( i == 1 )
            color = GREEN;
        else if ( i == 2 )
            color = YELLOW;
    }

  }

void PlayState::insertarBolaEscena (std::string nombreElemento) {

    if(_contadorBolas>1){
        std::string nombreAnt = std::string("");
        nombreAnt.append(nombreElemento);
        nombreAnt.append(Ogre::StringConverter::toString(_contadorBolas-2));
        _sceneMgr->getRootSceneNode()->removeAndDestroyChild(nombreAnt);
    }

    std::string nombre = std::string("");
    nombre.append(nombreElemento);
    nombre.append(Ogre::StringConverter::toString(_contadorBolas++));

    Ogre::Entity *entity = _sceneMgr->createEntity(nombre, nombreElemento + std::string(".mesh"));
    Ogre::SceneNode *node = _sceneMgr->createSceneNode(nombre);
    node->attachObject(entity);

    _sceneMgr->getRootSceneNode()->addChild(node);

    OgreBulletCollisions::SphereCollisionShape *ballShape = new OgreBulletCollisions::SphereCollisionShape(0.4);
    OgreBulletDynamics::RigidBody *rigidBall = new  OgreBulletDynamics::RigidBody(nombre, _world);

    Ogre::Vector3 pos = Ogre::Vector3 (0,5,30);
    rigidBall->setShape(node, ballShape, 0.6 /* Restitucion */, 0.6 /* Friccion */, 2.0 /* Masa */, pos, Quaternion::IDENTITY);

    rigidBall->enableActiveState();
    Ogre::Vector3 impulse = Ogre::Vector3(0.0 , 10.0 , -5.0);
    Ogre::Vector3 position = rigidBall->getCenterOfMassPosition();
    rigidBall->applyImpulse(impulse, position);
}

void PlayState::createScene()
{
    // Luz de la escena
    Ogre::Light* luz = _sceneMgr->createLight("Luz");
    luz->setType(Ogre::Light::LT_POINT);
    luz->setPosition(75,75,75);
    luz->setSpecularColour(1, 1, 1);
    luz->setDiffuseColour(1, 1, 1);
  }

void PlayState::update(double timeSinceLastFrame)
{
    if ( _empieza_a_contar )
        _tiempo += timeSinceLastFrame;

    Ogre::Vector3 vt(0,0,0);
    Ogre::Real deltaT = timeSinceLastFrame;
    bool endereza = false;

    _world->stepSimulation(deltaT); // Actualizar simulacion Bullet

    if ( _vCoches.size() > 0 )
    {
        _vCoches[0]->getVehiclePtr()->applyEngineForce ( 0, 0 );
        _vCoches[0]->getVehiclePtr()->applyEngineForce ( 0, 1 );

        //_vCoches[0]->accelerate ( true );

        if ( GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_UP ) )
        {
            //Si no se tienen pulsadas las teclas DER o IZQ ponemos las ruedas rectas
            if ( !GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_LEFT ) &&
                 !GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_RIGHT ) )
                endereza = true;

            //Se le aplica el empujón al coche
            _vCoches[0]->accelerate ( endereza );
        }
        else if ( GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_DOWN ) )
        {
            //Si no se tienen pulsadas las teclas DER o IZQ ponemos las ruedas rectas
            if ( !GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_LEFT ) &&
                 !GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_RIGHT ) )
                endereza = true;

            //Se le aplica el empujón al coche
            _vCoches[0]->decelerate ( endereza );
        }

        if ( GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_LEFT ) )
        {
            _vCoches[0]->turn_left();
        }
        else if ( GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_RIGHT ) )
        {
            _vCoches[0]->turn_right();
        }

    }
}
